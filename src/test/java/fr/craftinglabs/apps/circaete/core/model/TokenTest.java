package fr.craftinglabs.apps.circaete.core.model;

import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.Token;
import org.junit.Test;

import static java.util.UUID.fromString;
import static org.junit.Assert.*;

public class TokenTest {

    public static final String UUID = "d684a6e4-0e5d-4ab2-8e6d-b4bf95a581da";

    @Test public void
    should_have_an_encrypted_version_of_the_ChannelId() {
        ChannelId id = new ChannelId(fromString(UUID));
        Token token = new Token(id);

        assertEquals(UUID + "Encrypted", token.encrypted());
    }

    @Test public void
    should_validate_the_id_used_to_create_it() {
        ChannelId id = new ChannelId(fromString(UUID));
        Token token = new Token(UUID + "Encrypted");

        assertTrue(token.isValid(id));
    }

    @Test public void
    should_not_validate_another_id() {
        Token token = new Token(UUID + "Encrypted");
        ChannelId anotherId = new ChannelId();

        assertFalse(token.isValid(anotherId));
    }


}