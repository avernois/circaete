package fr.craftinglabs.apps.circaete.core.infrastructure.repository;

import fr.craftinglabs.apps.circaete.core.model.Channels;
import fr.craftinglabs.apps.circaete.core.model.ChannelsTest;

public class InMemoryChannelsTest extends ChannelsTest {

    @Override
    public Channels newChannel() {
        return new InMemoryChannels();
    }


}