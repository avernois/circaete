package fr.craftinglabs.apps.circaete.core.model;

import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.ChannelToken;
import fr.craftinglabs.apps.circaete.core.model.Token;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChannelTokenTest {

    @Test public void
    should_have_a_ChannelId() {
        ChannelId id = new ChannelId();
        Token token = new Token(id);
        ChannelToken channelToken = new ChannelToken(id, token);

        assertEquals(id, channelToken.id());
    }

    @Test public void
    should_have_a_Token() {
        ChannelId id = new ChannelId();
        Token token = new Token(id);
        ChannelToken channelToken = new ChannelToken(id, token);

        assertEquals(token, channelToken.token());
    }
}