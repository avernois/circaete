package fr.craftinglabs.apps.circaete.core.actions;

import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.Channels;
import fr.craftinglabs.apps.circaete.core.model.ChannelToken;
import fr.craftinglabs.apps.circaete.core.model.Token;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateAChannelTest {

    @Test public void
    should_return_channelId_and_a_token_when_creating_a_new_channel() {
        ChannelId id = new ChannelId();
        Channels channels = mock(Channels.class);
        when(channels.create()).thenReturn(id);
        CreateAChannel createAChannel = new CreateAChannel(channels);

        ChannelToken channelToken = createAChannel.create();

        Token token = new Token(id);
        assertEquals(new ChannelToken(id, token), channelToken);
    }
}
