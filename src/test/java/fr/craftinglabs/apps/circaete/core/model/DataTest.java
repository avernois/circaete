package fr.craftinglabs.apps.circaete.core.model;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class DataTest {
    @Test public void
    should_have_a_creation_date() {
        LocalDateTime now = LocalDateTime.now();

        Data data = new Data(now, new Key("key"), new Value("value"));

        assertEquals(now, data.receivedAt());
    }

    @Test public void
    should_have_a_key() {
        LocalDateTime now = LocalDateTime.now();
        Key key = new Key("key");

        Data data = new Data(now, key, new Value("value"));

        assertEquals(key, data.key());
    }

    @Test public void
    should_have_a_value() {
        LocalDateTime now = LocalDateTime.now();
        Key key = new Key("key");
        Value value = new Value("value");

        Data data = new Data(now, key, value);

        assertEquals(value, data.value());
    }


}