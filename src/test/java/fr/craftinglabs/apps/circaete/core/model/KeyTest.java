package fr.craftinglabs.apps.circaete.core.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KeyTest {
    @Test public void
    should_have_a_key() {
        Key key = new Key("key");

        assertEquals("key", key.key());
    }
}
