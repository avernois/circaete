package fr.craftinglabs.apps.circaete.core.model;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class ChannelIdTest {

    @Test public void
    should_have_an_id() {
        ChannelId id = new ChannelId();

        assertNotNull(id.id());
    }

    @Test public void
    should_create_an_id_from_a_UUID() {
        UUID uuid = UUID.randomUUID();
        ChannelId id = new ChannelId(uuid);

        assertEquals(uuid, id.id());
    }

}