package fr.craftinglabs.apps.circaete.core.actions;

import fr.craftinglabs.apps.circaete.core.model.*;
import fr.craftinglabs.apps.circaete.core.model.Token;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class StoreDataInAChannelTest {

    @Test public void
    should_store_a_data_into_a_channel() {
        Channels repository = mock(Channels.class);
        StoreDataInAChannel storeDataInAChannel = new StoreDataInAChannel(repository);
        ChannelId channelId = new ChannelId();
        Token token = new Token(channelId);
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        storeDataInAChannel.store(token, channelId, data);

        verify(repository).store(channelId, data);
    }

    @Test(expected = UnauthorizedToken.class) public void
    should_not_store_a_data_when_token_is_invalid() {
        Channels repository = mock(Channels.class);
        StoreDataInAChannel storeDataInAChannel = new StoreDataInAChannel(repository);
        ChannelId channelId = new ChannelId(UUID.fromString("d684a6e4-0e5d-4ab2-8e6d-b4bf95a581da"));
        Token token = new Token("WRONG!");
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        storeDataInAChannel.store(token, channelId, data);
    }
}
