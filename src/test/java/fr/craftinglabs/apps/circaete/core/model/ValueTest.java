package fr.craftinglabs.apps.circaete.core.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValueTest {

    @Test public void
    should_have_a_value() {
        Value value = new Value("value");

        assertEquals("value", value.value());
    }

}
