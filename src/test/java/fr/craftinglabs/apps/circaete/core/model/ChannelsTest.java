package fr.craftinglabs.apps.circaete.core.model;

import fr.craftinglabs.apps.circaete.core.actions.UnknownChannelException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public abstract class ChannelsTest {

    private Channels channels;

    @Before
    public void setUp() throws Exception {
        channels = newChannel();
    }

    @Test
    public void
    should_store_a_data_in_a_channel() {
        ChannelId channelId = channels.create();
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        channels.store(channelId, data);

        assertThat(channels.getDatasFrom(channelId), hasItem(data));
    }

    @Test public void
    should_return_all_stored_data_in_a_channel() {
        ChannelId channelId = channels.create();
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));
        Data anotherData = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        channels.store(channelId, data);
        channels.store(channelId, anotherData);

        assertThat(channels.getDatasFrom(channelId), hasItems(data, anotherData));
    }

    @Test public void
    should_not_return_data_stored_in_another_channel() {
        ChannelId channelId = channels.create();
        ChannelId anotherId = channels.create();
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        channels.store(anotherId, data);

        assertThat(channels.getDatasFrom(channelId), not(hasItem(data)));
    }

    @Test public void
    should_create_a_Channel() {
        ChannelId id = channels.create();

        assertNotNull(id);
    }

    @Test(expected = UnknownChannelException.class) public void
    should_throw_an_exception_when_asked_to_store_data_in_unknown_channel() {
        ChannelId id = new ChannelId();
        Data data = new Data(LocalDateTime.now(), new Key("key"), new Value("value"));

        channels.store(id, data);
    }


    public abstract Channels newChannel();

}
