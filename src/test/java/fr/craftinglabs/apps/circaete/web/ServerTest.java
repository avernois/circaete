package fr.craftinglabs.apps.circaete.web;

import fr.craftinglabs.apps.circaete.core.actions.CreateAChannel;
import fr.craftinglabs.apps.circaete.core.actions.StoreDataInAChannel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class ServerTest {

    private Server server;

    @Test public void
    should_send_Hello_World_on_get_hello() throws IOException {
        String content = getUri("http://localhost:4567/hello");

        assertEquals("Hello World", content);
    }

    @Before
    public void setUp() {
        StoreDataInAChannel storeDataInAChannel = mock(StoreDataInAChannel.class);
        CreateAChannel createAChannel = mock(CreateAChannel.class);
        server = new Server(storeDataInAChannel, createAChannel);
        server.start();
    }

    @After
    public void tearDown() throws InterruptedException {
        server.stop();
        // There is currently no way to know if server is really stopped. So wait and pray :(
        Thread.sleep(500);
    }

    private String getUri(String uri) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }
}
