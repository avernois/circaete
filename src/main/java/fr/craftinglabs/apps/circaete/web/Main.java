package fr.craftinglabs.apps.circaete.web;

import fr.craftinglabs.apps.circaete.core.actions.CreateAChannel;
import fr.craftinglabs.apps.circaete.core.actions.StoreDataInAChannel;
import fr.craftinglabs.apps.circaete.core.infrastructure.repository.InMemoryChannels;
import fr.craftinglabs.apps.circaete.core.model.Channels;

public class Main {
    public static void main(String[] args) {
        Channels channels = new InMemoryChannels();

        StoreDataInAChannel storeDataInAChannel = new StoreDataInAChannel(channels);
        CreateAChannel createAChannel = new CreateAChannel(channels);

        Server server = new Server(storeDataInAChannel, createAChannel);

        server.start();
    }
}
