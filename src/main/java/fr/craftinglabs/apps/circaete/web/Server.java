package fr.craftinglabs.apps.circaete.web;

import com.google.gson.Gson;
import fr.craftinglabs.apps.circaete.core.actions.CreateAChannel;
import fr.craftinglabs.apps.circaete.core.actions.StoreDataInAChannel;
import fr.craftinglabs.apps.circaete.core.actions.UnknownChannelException;
import fr.craftinglabs.apps.circaete.core.model.*;
import spark.Spark;

import java.time.LocalDateTime;
import java.util.UUID;

import static spark.Spark.*;

public class Server {

    private StoreDataInAChannel storeDataInAChannel;
    private CreateAChannel createAChannel;

    public Server(StoreDataInAChannel storeDataInAChannel, CreateAChannel createAChannel) {
        this.storeDataInAChannel = storeDataInAChannel;
        this.createAChannel = createAChannel;
    }

    public void start() {
        Gson gson = new Gson();
        get("/hello", (request, response) -> "Hello World");

        post("/:id/:key/:value", (request, response) -> {
            final Key key = new Key(request.params().get(":key"));
            final Value value = new Value(request.params().get(":value"));
            final Data data = new Data(LocalDateTime.now(), key, value);
            final ChannelId channelId = new ChannelId(UUID.fromString(request.params().get(":id")));
            final Token token = new Token(request.params().get(":token"));

            storeDataInAChannel.store(token, channelId, data);

            return "STORED " + data;
        });

        post("/channel/", ((request, response) -> createAChannel.create()), gson::toJson);

        exception(UnknownChannelException.class, (exception, request, response) -> {
            response.status(403);
            response.body("Unkwnown channel");
        });

        awaitInitialization();
    }

    public void stop() {
        Spark.stop();
    }
}
