package fr.craftinglabs.apps.circaete.core.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Data {
    private LocalDateTime receivedAt;
    private Key key;
    private Value value;

    public Data(LocalDateTime receivedAt, Key key, Value value) {
        this.receivedAt = receivedAt;
        this.key = key;
        this.value = value;
    }

    public LocalDateTime receivedAt() {
        return receivedAt;
    }

    public Key key() {
        return key;
    }

    public Value value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(receivedAt, data.receivedAt) &&
                Objects.equals(key, data.key) &&
                Objects.equals(value, data.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(receivedAt, key, value);
    }

    @Override
    public String toString() {
        return "Data{" +
                "receivedAt=" + receivedAt +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
