package fr.craftinglabs.apps.circaete.core.infrastructure.repository;

import fr.craftinglabs.apps.circaete.core.actions.UnknownChannelException;
import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.Channels;
import fr.craftinglabs.apps.circaete.core.model.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InMemoryChannels implements Channels {

    private HashMap<ChannelId, List<Data>> datas = new HashMap<>();
    private List<ChannelId> channels = new ArrayList<>();

    @Override
    public void store(ChannelId id, Data data) {
        if(!channels.contains(id))
            throw new UnknownChannelException();

        List<Data> list = datas.getOrDefault(id, new ArrayList<>());
        list.add(data);
        datas.put(id, list);
    }

    @Override
    public List<Data> getDatasFrom(ChannelId channelId) {
        return datas.get(channelId);
    }

    @Override
    public ChannelId create() {
        ChannelId id = new ChannelId();
        channels.add(id);

        return id;
    }
}
