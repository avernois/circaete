package fr.craftinglabs.apps.circaete.core.model;

import java.util.Objects;
import java.util.UUID;

public class ChannelId {
    private UUID id;

    public ChannelId(UUID id) {

       this.id = id;
    }

    public ChannelId() {
        this.id = UUID.randomUUID();
    }

    public UUID id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChannelId channelId = (ChannelId) o;
        return Objects.equals(id, channelId.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
