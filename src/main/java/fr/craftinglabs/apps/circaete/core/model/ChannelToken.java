package fr.craftinglabs.apps.circaete.core.model;

import java.util.Objects;

public class ChannelToken {
    private ChannelId id;
    private Token token;

    public ChannelToken(ChannelId id, Token token) {

        this.id = id;
        this.token = token;
    }

    public ChannelId id() {
        return id;
    }

    public Token token() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChannelToken that = (ChannelToken) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, token);
    }
}
