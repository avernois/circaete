package fr.craftinglabs.apps.circaete.core.model;

import java.util.List;

public interface Channels {

    void store(ChannelId id, Data data);

    List<Data> getDatasFrom(ChannelId channelId);

    ChannelId create();
}
