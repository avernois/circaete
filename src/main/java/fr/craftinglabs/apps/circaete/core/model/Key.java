package fr.craftinglabs.apps.circaete.core.model;

public class Key {
    private String key;

    public Key(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }

    @Override
    public String toString() {
        return "Key{" +
                "key='" + key + '\'' +
                '}';
    }
}
