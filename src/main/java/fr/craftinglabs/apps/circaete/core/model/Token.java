package fr.craftinglabs.apps.circaete.core.model;

import java.util.Objects;

public class Token {

    private final String encrypted;

    public Token(ChannelId id) {
        encrypted = encryptId(id);
    }

    public Token(String token) {
        encrypted = token;
    }

    public String encrypted() {
        return  encrypted;
    }

    public boolean isValid(ChannelId id) {
        return encrypted.equals(encryptId(id));
    }

    private String encryptId(ChannelId id) {
        return id.id().toString()+"Encrypted";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equals(encrypted, token.encrypted);
    }

    @Override
    public int hashCode() {

        return Objects.hash(encrypted);
    }
}
