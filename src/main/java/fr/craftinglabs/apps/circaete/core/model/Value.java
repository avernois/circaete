package fr.craftinglabs.apps.circaete.core.model;

public class Value {
    private String value;

    public Value(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "Value{" +
                "value='" + value + '\'' +
                '}';
    }
}
