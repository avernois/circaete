package fr.craftinglabs.apps.circaete.core.actions;

import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.Channels;
import fr.craftinglabs.apps.circaete.core.model.Data;
import fr.craftinglabs.apps.circaete.core.model.Token;

public class StoreDataInAChannel {
    private Channels repository;

    public StoreDataInAChannel(Channels repository) {

        this.repository = repository;
    }

    public void store(Token token, ChannelId channelId, Data data) {
        if(!token.isValid(channelId))
            throw new UnauthorizedToken();

        repository.store(channelId, data);
    }
}
