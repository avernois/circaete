package fr.craftinglabs.apps.circaete.core.actions;

import fr.craftinglabs.apps.circaete.core.model.ChannelId;
import fr.craftinglabs.apps.circaete.core.model.Channels;
import fr.craftinglabs.apps.circaete.core.model.ChannelToken;
import fr.craftinglabs.apps.circaete.core.model.Token;

public class CreateAChannel {

    private Channels channels;

    public CreateAChannel(Channels channels) {
        this.channels = channels;
    }

    public ChannelToken create() {
        ChannelId id = channels.create();
        Token token = new Token(id);

        return new ChannelToken(id, token);
    }
}
